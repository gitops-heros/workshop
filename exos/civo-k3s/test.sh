#!/bin/bash




set +e 
civo firewall rule ls kube-firewalX &>/dev/null
RET=$?

set -e
echo $RET

if [ "x$RET" == "x1" ]
then
    echo "Pas de firewall"
fi