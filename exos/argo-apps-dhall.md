### Intégration de dhall dans argocd
Argocd gère plusieurs systèmes de deploiement : yaml "vanilla", kustomize, helm, jsonnet, mais pas dhall. Mais heuresement votre consultant Beornide à préparer un CMDB et sa configuration 

On peut étendre argocd via un mécanisme de plugin, avec ce que l'on appel des CMP ou Config Management Plugin.  Il y a deux manière de faire cela, dans notre cas nous allons utilisé un mécanisme de side car avec un container side car qui sera ajouté au pod "argo-repo-server" (voir https://argo-cd.readthedocs.io/en/stable/user-guide/config-management-plugins/).

Pour charger cette intégration executer la commande suivante :
```
kubectl apply -f argo-dhall\0-install-2.3.4-patched.yaml
```
### Utilisation de dhall dans argocd

Le descripteur d'`Application` ArgoCD pour dhall est un descripteur tradictionnel enrichie avec une variable d'environnement qui indique quelle "fichier de composition" il faut utiliser pour l'environnement

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: dhall-demo
  namespace: argocd
spec:
  destination:
    namespace: dhall-demo
    server: 'https://kubernetes.default.svc'
  source:
    path: dhall
    repoURL: 'https://github.com/louiznk/deploy-demo.git'
    targetRevision: main
    plugin:
      env:
        - name: DHALL_FILE
          value: assembly-demo.dhall
  project: default
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
```
Dans cette exemple les descripteurs dhall sont dans https://github.com/louiznk/deploy-demo/dhall et dans ce dossier le fichier dhall qui sera compilé (puis permettra le déploiement) est `assembly-demo.dhall`

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern
