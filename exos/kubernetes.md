# Installation d'un cluster civo

## Configurer la cmd civo

```shell
civo apikey save NAME APIKEY
```
On trouve APIKEY sur son compte [civo](https://www.civo.com/account/security)

Choisissez ensuite la région par défaut
```shell
civo region ls

civo region current LON1
```

## Créer le cluster
Trouvez un petit nom pour votre cluster....
```shell
export clustername=AZERTYUIOP
```


### Version Distante :CIVO

#### En commande line

1) Créer un cluster 
Sans Traefik, avec 2 nodes de taille medium en métant à jours votre "KUBECONFIG" (~/.kube/config) avec cette nouvelle entrée
```shell
civo k3s create ${clustername} --remove-applications=Traefik --nodes 2 --size "g3.k3s.medium" --wait --save --merge --yes
```

Vérifiez que votre cluster est accessible
```shell
kubectl version
kubectl cluster-info
```

Notes :  
Un firewall est automatiquement créer, vous auriez pu en créer un que vous réutiliser. Vous pouvez aussi modifier celui créer.  
A la suppression du cluster pensez à supprimer le firewall

1) Installer traefik
(le chemin du dossier traefik dépend de vous)
```
kubectl apply -f ./civo-k3s/traefik/ --wait
```

Vérifiez que tous vos POD dont Traefik sont up & running
```
kubectl get pod -A
```
#### OU avec 1 seul script
Depuis le répertoire civo-k3s
```
./create-cluster.sh $clustername
```

### Version local avec k3d
<!> Marche seulement depuis un Linux à vous, pas depuis gitpods
<!> Nécessite d'avoir docker, kubectl et k3d d'installer

Depuis le répertoire local-k3d
```shell
./create_cluster.sh $clustername
```
Et normalement c'est finit


## Récupérez l'IP de cluster
Cela vous sera utile pour la suite.  
Regardez dans le fichier de "KUBECONFIG" ou avec la cmd kubectl cluster-info

TIPS
```
export IP=$(kubectl cluster-info | head -n 1 | cut -d'/' -f3  | cut -d":" -f1)
echo "L'ip du cluster est $IP"
```

Normalement curl sur l'IP doit renvoyer une 404 (à la fois sur le port 80 et le 443)